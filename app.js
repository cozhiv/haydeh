const express = require("express");
const exphbs = require('express-handlebars');
const mongoose = require("mongoose");

const port = 5002;
const app = express();


// Parse request body as JSON
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(express.static("public"));

//Map global promise - get rid of warning
//mongoose.Promise = global.Promise;
//Connect to mongoose
mongoose.connect('mongodb://localhost/kkok', {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).catch(function(error){
  console.log(error);
});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'db connection error:'));
db.once('open', function() {
  // we're connected!
  console.log("MongoDB connected ...");
});
// accounts model
//require(".models/Account");

const models = require("./models");

//Handlebars middleware
app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');



//Index Route
app.get("/", (req, res) => {
    const rightHere = new Date();
    console.log(rightHere.toTimeString);
    const localVar = "Hay!, ";
    res.render('index', {
            shy: localVar
    });
});

app.get("/about", (req, res) => {
 res.render("about");
});

app.get("/help", (req, res) => {
    res.render("help");
});

app.get("/products", function(req, res) {
    models.Product.find({})
    .then(function(mProducts) {
      res.json(mProducts);
    })
    .catch(function(err) {
      res.json(err);
    })
  });
// Route to get all chats
  app.get("/chats", function(req, res) {
    models.Chat.find({})
    .then(function(mChats) {
      res.json(mChats);
    })
    .catch(function(err) {
      res.json(err);
    })
  });
	
// Route for creating a new Product
  app.post("/product", function(req, res) {
    models.Product.create(req.body)
      .then(function(mProduct) {
        // If we were able to successfully create a Product, send it back to the client
        res.json(mProduct);
      })
      .catch(function(err) {
        // If an error occurred, send it to the client
        res.json(err);
      });
  });

// Route for creating a new Review and updating Product "review" field with it
app.post("/product/:id", function(req, res) {
    // Create a new note and pass the req.body to the entry
    models.Chat.create(req.body)
      .then(function(mChat) {
        // If a Review was created successfully, find one Product with an `_id` equal to `req.params.id`. Update the Product to be associated with the new Review
        // { new: true } tells the query that we want it to return the updated Product -- it returns the original by default
        // Since our mongoose query returns a promise, we can chain another `.then` which receives the result of the query
        return models.Product.findOneAndUpdate({ _id: req.params.id }, { chats: mChat._id }, { new: true });
      })
      .then(function(mProduct) {
        // If we were able to successfully update a Product, send it back to the client
        res.json(mProduct);
      })
      .catch(function(err) {
        // If an error occurred, send it to the client
        res.json(err);
      });
  });

// Route for retrieving a Product by id and populating it's Review.
app.get("/products/:id", function(req, res) {
    // Using the id passed in the id parameter, prepare a query that finds the matching one in our db...
    models.Product.findOne({ _id: req.params.id })
      // ..and populate all of the notes associated with it
      .populate("chats")
      .then(function(mProduct) {
        // If we were able to successfully find an Product with the given id, send it back to the client
        res.json(mProduct);
      })
      .catch(function(err) {
        // If an error occurred, send it to the client
        res.json(err);
      });
  });



app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});

