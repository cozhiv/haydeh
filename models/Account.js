const mongoose = require("mongoose");

//Account Schema
const AccountSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    generated: {
        type: Date,
        default: Date.now(),
    },
    tools: [{
        type: Schema.Types.ObjectId,
        ref: "Tool"
    }],
    chats: [{
        type: Schema.Types.ObjectId,
        ref: "Chat"
      }]
});
AccountSchema.methods.say = function () {
    const greeting = this.firstName
    ? "Meow name is " + this.firstName
    : "I don't have a name";
  console.log(greeting);
};
// AccountSchema.find({firstName: /^f/}, function(){

// })
// const fluffy = new AccountSchema({ firstName: 'fluffy' });
// fluffy.save(function (err, fluffy) {
//     if (err) return console.error(err);
//     fluffy.say();
//   });
const Account = mongoose.model("Account", AccountSchema);
module.exports = Account;