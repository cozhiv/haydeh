const mongoose = require("mongoose");

// Get the Schema constructor
const Schema = mongoose.Schema;

// Using Schema constructor, create a ProductSchema
const ChatSchema = new Schema({
  stars: {
    type: Number,
    required: true
  },
  review: {
    type: String,
    required: true
  },
  accounts: [{
    type: Schema.Types.ObjectId,
    ref: "Account"
  }],
  // messages: [{
  //   sender:String,
    
  // }]
});

// Create model from the schema
const Chat = mongoose.model("Chat", ChatSchema);

// Export model
module.exports = Chat;