module.exports = {
    Chat: require("./Chat"),
    Product: require("./Product"),
    Account: require("./Account")
};