const mongoose = require("mongoose");
const db = require("./models");

const createAccount = function(acc) {
  return db.Account.create(acc).then(docAccount => {
    console.log("\n>> Created Account:\n", docAccount);
    return docAccount;
  });
};

const createChat = function(ch) {
  return db.Chat.create(ch).then(docChat => {
    console.log("\n>> Created Chat:\n", docChat);
    return docChat;
  });
};

const addChatToAccount = function(accountId, chat) {
  return db.Account.findByIdAndUpdate(
    accountId,
    { $push: { chats: chat._id } },
    { new: true, useFindAndModify: false }
  );
};

const addAccountToChat = function(chatId, account) {
  return db.Chat.findByIdAndUpdate(
    chatId,
    { $push: { accounts: account._id } },
    { new: true, useFindAndModify: false }
  );
};

const getTutorialWithPopulate = function(id) {
return db.Account.findById(id).populate("chats", "-_id -__v -accounts");
};
  
const getTagWithPopulate = function(id) {
  return db.Chat.findById(id).populate("accounts", "-_id -__v -chats");
};
  




const run = async function() {

  var acc1 = await createAccount({
    title: "many to many chats accounts",
    author: "hg"
  });

  var chatA = await createChat({
    name: "chatA",
    review: "chat-a"
  });

  var chatB = await createChat({
    name: "chatB",
    review: "chat-b"
  });

  var account = await addChatToAccount(acc1._id, chatA);
  console.log("\n>> acc1:\n", account);

  var chat = await addAccountToChat(chatA._id, acc1);
  console.log("\n>> chatA:\n", chat);

  account = await addChatToAccount(acc1._id, chatB);
  console.log("\n>> acc1:\n", account);

  chat = await addAccountToChat(chatB._id, acc1);
  console.log("\n>> chatB:\n", chat);

  var chat2 = await createAccount({
    title: "Tut #2",
    author: "hg"
  });

  account = await addChatToAccount(chat2._id, chatB);
  console.log("\n>> chat2:\n", account);

  chat = await addAccountToChat(chatB._id, chat2);
  console.log("\n>> chatB:\n", chat);
};

// mongoose
//   .connect("mongodb://localhost/bezkoder_db", {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
//   })
//   .then(() => console.log("Successfully connect to MongoDB."))
//   .catch(err => console.error("Connection error", err));

//run();
